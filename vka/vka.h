#pragma once
#ifndef VKA_H_
#define VKA_H_


#include <vka/core/context.h>
#include <vka/core2/BufferMemoryPool.h>
#include <vka/core2/CommandBuffer.h>
#include <vka/core2/CommandPool.h>
#include <vka/core2/DescriptorLayoutSet.h>
#include <vka/core2/DescriptorPool.h>
#include <vka/core2/DescriptorSet.h>
#include <vka/core2/Memory.h>
#include <vka/core2/MeshObject.h>
#include <vka/core2/Pipeline.h>
#include <vka/core2/RenderTarget.h>
#include <vka/core2/Screen.h>
#include <vka/core2/Semaphore.h>
#include <vka/core2/Shader.h>
#include <vka/core2/TextureMemoryPool.h>


#endif
